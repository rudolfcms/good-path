<?php

use Rudolf\Component\Hooks;

Hooks\Filter::add('content_filter', function ($text) {
    $text = str_replace('href="/', 'href="'.DIR.'/', $text);
    $text = str_replace('src="/', 'src="'.DIR.'/', $text);
    $text = str_replace(DIR.DIR, DIR, $text);

    return $text;
});
